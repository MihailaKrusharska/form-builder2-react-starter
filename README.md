# Homework - Dynamic Form Builder with React Starter

This project set ups React project from scratch.

## Deployed project

The deployed application can be found [here](https://mihailakrusharska.gitlab.io/form-builder-react-starter/).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.

### `npm run build`

Builds the app for production to the `dist` folder.\
Your app is ready to be deployed!

### `npm lint`

Launches eslint without fixing errors.

### `npm lint:fix`

Launches eslint and fixes errors.

### `npm format`

Launches prettier.
