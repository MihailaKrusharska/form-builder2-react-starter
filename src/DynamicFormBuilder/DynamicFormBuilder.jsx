import React, { useEffect, useState } from 'react';

const DynamicFormBuilder = ({ formJSON, onSubmit }) => {
  const [formData, setFormData] = useState(
    formJSON[0].fields.reduce((acc, field) => {
      return { ...acc, [field['id']]: field['value'] };
    }, {}),
  );

  const [visibleFields, setVisibleFields] = useState(
    formJSON[0].fields.reduce((acc, field) => {
      return {
        ...acc,
        [field['id']]: field['visible'] === false ? false : true,
      };
    }, {}),
  );

  useEffect(() => {
    const newVisibleFields = {
      ...visibleFields,
      email: formData.subscribe ? true : false,
    };
    setVisibleFields(newVisibleFields);
  }, [formData]);

  const handleInputChange = (fieldId, value) => {
    setFormData({ ...formData, [fieldId]: value });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();

    // Validate required fields
    const requiredFields = formJSON[0].fields.filter(
      (field) => field.required && !formData[field.id] && visibleFields[field.id] === true,
    );
    if (requiredFields.length > 0) {
      alert('Please fill in all required fields.');
      return;
    }
    onSubmit(formData);
  };

  const renderInput = (field) => {
    return (
      <>
        <label htmlFor={field.id} className="form-label">
          {field.label} {field.required && <span className="text-danger">*</span>}
        </label>
        <input
          type="text"
          className="form-control"
          id={field.id}
          placeholder={field.placeholder}
          value={formData[field.id] || ''}
          onChange={(e) => handleInputChange(field.id, e.target.value)}
          required={field.required}
        />
      </>
    );
  };

  const renderSelect = (field) => {
    return (
      <>
        <label htmlFor={field.id} className="form-label">
          {field.label} {field.required && <span className="text-danger">*</span>}
        </label>
        <select
          className="form-select"
          id={field.id}
          value={formData[field.id] || ''}
          onChange={(e) => handleInputChange(field.id, e.target.value)}
          required={field.required}
        >
          <option value="" disabled>
            Select {field.label.toLowerCase()}
          </option>
          {field.options.map((option, optionIndex) => (
            <option key={optionIndex} value={option.label}>
              {option.label}
            </option>
          ))}
        </select>
      </>
    );
  };

  const renderCheckbox = (field) => {
    return (
      <div className="form-check">
        <input
          type="checkbox"
          className="form-check-input"
          id={field.id}
          checked={formData[field.id] || false}
          onChange={(e) => handleInputChange(field.id, e.target.checked)}
        />
        <label className="form-check-label" htmlFor={field.id}>
          {field.label}
        </label>
      </div>
    );
  };

  return (
    <form onSubmit={handleFormSubmit}>
      {formJSON.map((section, index) => (
        <div key={index} className="mb-3">
          {section.fields.map(
            (field) =>
              visibleFields[field.id] !== false && (
                <div key={field.id} className="mb-3">
                  {field.type === 'text' && renderInput(field)}
                  {field.type === 'select' && renderSelect(field)}
                  {field.type === 'checkbox' && renderCheckbox(field)}
                </div>
              ),
          )}
        </div>
      ))}
      <button type="submit" className="btn btn-primary">
        Submit
      </button>
    </form>
  );
};

export default DynamicFormBuilder;
