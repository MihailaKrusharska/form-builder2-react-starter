import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App.jsx';

const rootContainer = document.getElementById('app');
const app = createRoot(rootContainer);
app.render(<App />);
