import React from 'react';
import './App.css';
import DynamicFormBuilder from './DynamicFormBuilder/DynamicFormBuilder.jsx';
import jsonData from './jsonData';

function App() {
  const handleSubmit = (data) => {
    console.log(data);
  };

  return (
    <div className="App">
      <div className="formcontainer container">
        <div className="formouter w-50">
          <DynamicFormBuilder formJSON={jsonData} onSubmit={handleSubmit} />
        </div>
      </div>
    </div>
  );
}

export default App;
